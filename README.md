### feed-prpl-abandoned

This is an package feed containing packages which are no longer being used in
prplOS and thus considered abondoned. That's it, packages which are no longer
maintained and thus not supported.

To use these packages, add the following line to the feeds.conf
in the prplOS buildroot:

  src-git prpl-abandoned https://gitlab.com/prpl-foundation/prplos/feed-prpl-abandoned

Update the feed:

  ./scripts/feeds update prpl-abandoned

Activate the packages:

  ./scripts/feeds install -a -p prpl-abandoned

The extra packages should now appear in menuconfig.
